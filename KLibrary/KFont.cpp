#include <cassert>
#include <iostream>
#include "KFont.hpp"

KFont::KFont() : mFont(nullptr) {
}

KFont::KFont(const std::string& file, int size) : KFont() {
    mFont = TTF_OpenFont(file.c_str(), size);
}

KFont::~KFont() {
    if (mFont != nullptr) {
        TTF_CloseFont(mFont);
    }
}

bool KFont::is_opened() {
    return mFont != nullptr;
}

KSurface KFont::operator()(const std::string& text, KColor color) {
    assert(mFont != nullptr);
    return KSurface(TTF_RenderText_Blended(mFont, text.c_str(), color.to_SDL_Color()));
}
