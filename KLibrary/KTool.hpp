#ifndef KTool_HEADER_DEFINED
#define KTool_HEADER_DEFINED

#include <cassert>
#include <algorithm>

namespace KTool {
    template<typename T>
    int lenof(const T& tab) {
        return sizeof tab / sizeof *tab;
    }

    template<int base = 10, typename T>
    char* itoa(T num, char* str, int size) {
        assert(size >= 2 && base >= 2);

        int i = 0;
        bool isNegative = false;

        /* Handle 0 explicitely, otherwise empty string is printed for 0 */
        if (num == 0) {
            str[0] = '0';
            str[1] = '\0';
            return str;
        }

        // In standard itoa(), negative numbers are handled only with
        // base 10. Otherwise numbers are considered unsigned.
        if (num < 0 && base == 10) {
            isNegative = true;
            num = -num;
        }

        // Process individual digits
        while (num != 0 && i < size) {
            int rem = num % base;
            str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
            num = num/base;
        }

        // If number is negative, append '-'
        if (isNegative && i < size) {
            str[i++] = '-';
        }

        if (i >= size) {
            str[0] = '0';
            str[1] = '\0';
            return str;
        }
        str[i] = '\0'; // Append string terminator

        // Reverse the string
        int start = 0;
        int end = i - 1;
        while (start < end) {
            std::swap(*(str+start), *(str+end));
            start++;
            end--;
        }

        return str;
    }

}

#endif
