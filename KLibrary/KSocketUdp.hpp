#ifndef KSocketUdp_HEADER_DEFINED
#define KSocketUdp_HEADER_DEFINED

#include "KSocket.hpp"

class KSocketUdp : public KSocket {
    virtual void abstract() {return;};
    void reset(Propertie propertie);
public:
    KSocketUdp(Propertie propertie = 0);
    KSocketUdp(IpVersion ipVersion, Propertie propertie = 0);// +open(ipVersion)
    KSocketUdp(const Addr& address, Propertie propertie = 0);// +open(address)
    ~KSocketUdp();
    bool open(IpVersion = IpvX);
    bool open(const Addr& address);// +bind(address)
    void close();
    int  read(                            void* buffer, int size);
    int  read_and_catch(                  void* buffer, int size);
    int  read_and_catch(Addr& address,    void* buffer, int size);
    int  read(          Addr& address,    void* buffer, int size);
    int  write(                     const void* buffer, int size);
    int  write(const Addr& address, const void* buffer, int size);
    int  write(const Addr& address, const char* message);
    int  write(                     const char* message);
    void set_peer_addr(const Addr& address);
};

std::ostream& operator<<(std::ostream &ostream, KSocketUdp& socket);
std::istream& operator>>(std::istream &istream, KSocketUdp& socket);

#endif
