#ifndef KSocket_HEADER_DEFINED
#define KSocket_HEADER_DEFINED

#include <string>
#include <sys/socket.h>

class KSocket {
    KSocket(const KSocket& src) = delete;
    virtual void abstract() = 0;
public:
    typedef int           IpVersion;
    typedef unsigned char Propertie;

    static constexpr Propertie Verbose   = 1<<0;
    static constexpr Propertie Blocking  = 1<<1;
    static constexpr Propertie ReuseAddr = 1<<2;
    static constexpr Propertie Broadcast = 1<<3;

    static constexpr int PortLenght = 6;
    static constexpr int AddrLenght = 54;
    static constexpr int Closed     = -1;

    static constexpr IpVersion IpvX = AF_UNSPEC;
    static constexpr IpVersion Ipv4 = AF_INET;
    static constexpr IpVersion Ipv6 = AF_INET6;

    static bool print_interface_address();
    class Addr : public sockaddr_storage {
    public:
        Addr();
        Addr(const sockaddr*         address);
        Addr(const sockaddr_storage& address);
        Addr(const char *        address, unsigned short port = 0);
        Addr(const std::string & address, unsigned short port = 0);
        void clear();
        const sockaddr_storage* get_struct_ptr() const;
        const sockaddr*         get_addr_ptr()   const;
        unsigned int            get_len()        const;
        unsigned short          get_port()       const;
        IpVersion               get_ip_version() const;
        bool                    is_set()         const;
        operator                std::string()    const;
        std::string to_string(bool port = true)  const;
    };
    KSocket(Propertie propertie = 0);
    bool bind(const Addr& address);
    bool      is_opened()      const;
    bool      is_closed()      const;
    Addr      get_bind_addr()  const;
    Addr      get_peer_addr()  const;
    IpVersion get_ip_version() const;
    unsigned short get_port()  const;
    bool set_propertie(Propertie propertie);
    bool set_propertie(bool set, Propertie propertie);
    void verbose(const std::string & str)            const;
    void verbose(const std::string & str, int state) const;
    bool join_membership(const Addr& address);
    bool drop_membership(const Addr& address);
protected:
    int  mSocket;
    Addr mBindAddr;
    Addr mPeerAddr;
    Propertie mPropertie;
    void reset(Propertie propertie);
    bool init_propertie();
private:
    bool apply_propertie_blocking(  bool set);
    bool apply_propertie_broadcast( bool set);
    bool apply_propertie_reuse_addr(bool set);
};

std::ostream& operator<<(std::ostream& ostream, const KSocket::Addr& addr);
std::istream& operator>>(std::istream& istream,       KSocket::Addr& addr);

#endif
