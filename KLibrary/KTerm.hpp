#ifndef KTerm_HEADER_DEFINED
#define KTerm_HEADER_DEFINED

namespace KTerm {
    constexpr const char Def[]     = "\e[0m";
    constexpr const char Bold[]    = "\e[1m";
    constexpr const char Dim[]     = "\e[2m";
    constexpr const char Invert[]  = "\e[7m";
    constexpr const char UBold[]   = "\e[21m";
    constexpr const char UDim[]    = "\e[22m";
    constexpr const char UInvert[] = "\e[27m";
    constexpr const char UColor[]  = "\e[39m";
    constexpr const char Black[]   = "\e[30m";
    constexpr const char Red[]     = "\e[31m";
    constexpr const char Green[]   = "\e[32m";
    constexpr const char Yellow[]  = "\e[33m";
    constexpr const char Blue[]    = "\e[34m";
    constexpr const char Magenta[] = "\e[35m";
    constexpr const char Cyan[]    = "\e[36m";
    constexpr const char Gray[]    = "\e[37m";
    /* templates are awesome ! */
    template<unsigned char colorId> constexpr const char Color[12] = {
        '\e', '[', '3', '8', ';', '5', ';',
        /*               ( \e[38;5;Xm  )                      ( \e[38;5;XXm      )   ( \e[38;5;XXXm          ) */
        (colorId < 10) ? (colorId + '0') : ((colorId < 100) ? (colorId / 10 + '0') : (colorId / 100 + '0'    )),
        (colorId < 10) ? ('m'          ) : ((colorId < 100) ? (colorId % 10 + '0') : (colorId / 10 % 10 + '0')),
        (colorId < 10) ? ('\0'         ) : ((colorId < 100) ? ('m'               ) : (colorId % 10 + '0'     )),
        colorId >= 100 ? 'm' : '\0', '\0'
    };
    template<unsigned char code> constexpr const char Code[7] = {
        '\e', '[',
        /*            ( \e[Xm    )                   ( \e[XXm        )   ( \e[XXXm            ) */
        (code < 10) ? (code + '0') : ((code < 100) ? (code / 10 + '0') : (code / 100 + '0'    )),
        (code < 10) ? ('m'       ) : ((code < 100) ? (code % 10 + '0') : (code / 10 % 10 + '0')),
        (code < 10) ? ('\0'      ) : ((code < 100) ? ('m'            ) : (code % 10 + '0'     )),
        code >= 100 ? 'm' : '\0', '\0'
    };
}

#endif
