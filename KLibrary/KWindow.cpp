#include "KWindow.hpp"
#include "KTerm.hpp"
#include <iostream>

KWindow::KWindow() :
    mWindow(nullptr),
    mRenderer(nullptr),
    mName(),
    mHidden(true),
    w(0),
    h(0),
    id(0)
{}

KWindow::KWindow(const std::string& name, int width, int height, Flags flags) :
    KWindow()
{
    open(name, width, height, flags);
}

KWindow::~KWindow() {
    close();
}

void KWindow::show() {
    assert(mWindow != nullptr);
    if (mHidden == true) {
        SDL_ShowWindow(mWindow);
        verbose("show");
        mHidden = false;
    }
}

void KWindow::hide() {
    assert(mWindow != nullptr);
    if (mHidden == false) {
        SDL_HideWindow(mWindow);
        verbose("hide");
        mHidden = true;
    }
}

void KWindow::verbose(const std::string& message) {
    std::cout << KTerm::Bold << "[" << KTerm::Color<204> << "KWindow" << KTerm::UColor
    << "] " << KTerm::UBold << KTerm::Color<204> << message << KTerm::Def << std::endl;
}

bool KWindow::is_opened() {
    return mWindow != nullptr;
}

bool KWindow::is_closed() {
    return mWindow == nullptr;
}

bool KWindow::open(const std::string& name, int width, int height, Flags flags) {
    assert(is_closed());
    mWindow = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, flags);
    if (mWindow == nullptr) {
        return false;
    }
    SDL_SetWindowData(mWindow, "this", this);
    mRenderer = SDL_CreateRenderer(mWindow, -1, 0);
    if (mRenderer == nullptr) {
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;
        return false;
    }
    verbose("opened");
    mName = name;
    *((int*)&w ) = width;
    *((int*)&h ) = height;
    *((int*)&id) = SDL_GetWindowID(mWindow);
    return true;
}

void KWindow::display() {
    assert(mRenderer != nullptr);
    SDL_RenderPresent(mRenderer);
}

void KWindow::set_alpha_blend(bool set) {
    assert(mRenderer != nullptr);
    SDL_SetRenderDrawBlendMode(mRenderer, set ? SDL_BLENDMODE_BLEND : SDL_BLENDMODE_NONE);
}

void KWindow::close() {
    if (mRenderer != nullptr) {
        SDL_DestroyRenderer(mRenderer);
        mRenderer = nullptr;
    }
    if (mWindow != nullptr) {
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;
        *((int*)&id) = 0;
        verbose("closed");
    }
}

KTexture KWindow::texture(const KSurface& src) {
    assert(mRenderer != nullptr);
    return KTexture(SDL_CreateTextureFromSurface(mRenderer, src.get_ptr()), src.w, src.h);
}

KTexture KWindow::texture(const std::string& fileName) {
    return texture(KSurface(fileName));
}

void KWindow::print(const KTexture& src, int x, int y) {
    SDL_Rect rect = {
        x,
        h - y - src.h,
        src.w,
        src.h
    };
    assert(src.get_ptr() != nullptr);
    SDL_RenderCopy(mRenderer, src.get_ptr(), nullptr, &rect);
}

void KWindow::print(const KTexture& src, int x, int y, int w, int h) {
    SDL_Rect rect = {
        x,
        this->h - y - h,
        w,
        h
    };
    assert(src.get_ptr() != nullptr);
    SDL_RenderCopy(mRenderer, src.get_ptr(), nullptr, &rect);
}

void KWindow::fill(const KRect<>& rect, KColor color) {
    assert(mRenderer != nullptr);
    SDL_SetRenderDrawColor(mRenderer, color.r, color.g, color.b, color.a);
    SDL_Rect sdlRect = {
        rect.x,
        h - rect.y - rect.h,
        rect.w,
        rect.h
    };
    SDL_RenderFillRect(mRenderer, &sdlRect);
}
void KWindow::fill(KColor color) {
    assert(mRenderer != nullptr);
    SDL_SetRenderDrawColor(mRenderer, color.r, color.g, color.b, color.a);
    SDL_RenderClear(mRenderer);
}

void KWindow::draw_line(int x1, int y1, int x2, int y2, KColor color) {
    assert(mRenderer != nullptr);
    SDL_SetRenderDrawColor(mRenderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawLine(mRenderer, x1, h - y1, x2, h - y2);
}

void KWindow::draw_rect(const KRect<>& rect, KColor color) {
    assert(mRenderer != nullptr);
    SDL_SetRenderDrawColor(mRenderer, color.r, color.g, color.b, color.a);
    SDL_Rect sdlRect = {
        rect.x,
        h - rect.y - rect.h,
        rect.w,
        rect.h
    };
    SDL_RenderDrawRect(mRenderer, &sdlRect);
}

void KWindow::update_size() {
    SDL_GetWindowSize(mWindow, (int*)(&w), (int*)(&h));
}

void KWindow::resize(int width, int height) {
    assert(mWindow != nullptr);
    *((int*)(&w)) = width;
    *((int*)(&h)) = height;
    SDL_SetWindowSize(mWindow, w, h);
    mRenderer = SDL_GetRenderer(mWindow);
}
