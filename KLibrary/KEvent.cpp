#include "KEvent.hpp"
#include "KWindow.hpp"

KEvent::KEvent() {
    memset(this, 0, sizeof *this);
}

void KEvent::update(bool repeat) {
    memset(this, 0, sizeof *this);
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_KEYDOWN:
            key[event.key.keysym.scancode] = repeat || !event.key.repeat;
            break;
            case SDL_WINDOWEVENT:
            if (event.window.event == SDL_WINDOWEVENT_CLOSE) {
                close = event.window.windowID;
            } else
            if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                ((KWindow*)SDL_GetWindowData(
                    SDL_GetWindowFromID(event.window.windowID),
                    "this")
                )->update_size();
            }
            break;
            case SDL_MOUSEBUTTONDOWN:
            switch (event.button.button) {
                case SDL_BUTTON_LEFT:
                mouseLeft.clicked = true;
                mouseLeft.x = event.button.x;
                mouseLeft.y = event.button.y;
                break;
                case SDL_BUTTON_MIDDLE:
                mouseMiddle.clicked = true;
                mouseMiddle.x = event.button.x;
                mouseMiddle.y = event.button.y;
                break;
                case SDL_BUTTON_RIGHT:
                mouseRight.clicked = true;
                mouseRight.x = event.button.x;
                mouseRight.y = event.button.y;
                break;
            }
        }
    }
}

bool KEvent::operator[](KKey::Id keyId) const {
    return key[SDL_GetScancodeFromKey(keyId)];
}
