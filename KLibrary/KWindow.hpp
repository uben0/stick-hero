#ifndef KWindow_HEADER_DEFINED
#define KWindow_HEADER_DEFINED

#include "KLib_SDL.hpp"
#include <cassert>
#include <string>
#include "KRect.hpp"
#include "KColor.hpp"
#include "KSurface.hpp"
#include "KTexture.hpp"

class KWindow {
    static constexpr unsigned char type = 0;
    KLib_SDL      sdl_init;
    SDL_Window*   mWindow;
    SDL_Renderer* mRenderer;
    std::string   mName;
    bool          mHidden;
    void verbose(const std::string& message);
public:
    typedef Uint32 Flags;
    static constexpr Flags Hidden    = SDL_WINDOW_HIDDEN;
    static constexpr Flags Resizable = SDL_WINDOW_RESIZABLE;

    const int w, h;
    const unsigned int id;

    KWindow();
    KWindow(const std::string& name, int width, int height, Flags flags = 0);
    ~KWindow();

    bool open(const std::string& name, int width, int height, Flags flags = 0);
    void close();

    bool is_opened();
    bool is_closed();

    void show();
    void hide();

    void update_size();
    void resize(int w, int h);
    void set_alpha_blend(bool set);
    void fill(const KRect<>& rect, KColor color);
    void fill(                     KColor color);
    void draw_line(int x1, int y1, int x2, int y2, KColor color);
    void draw_rect(const KRect<>& rect, KColor color);

    KTexture texture(const KSurface& src);
    KTexture texture(const std::string& fileName);
    void print(const KTexture& src, int x, int y);
    void print(const KTexture& src, int x, int y, int w, int h);

    void display();
};

#endif
