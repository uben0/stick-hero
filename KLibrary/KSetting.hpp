#ifndef KSetting_HEADER_DEFINED
#define KSetting_HEADER_DEFINED

#include <string>
#include <fstream>
#include <sstream>

class KAbstractSetting {
protected:
    KAbstractSetting(const std::string& name) : name(name) {}
public:
    virtual ~KAbstractSetting() {}
    const std::string name;
    virtual bool load(std::istream& file) = 0;
    virtual void save(std::ostream& file) = 0;
};

template<typename T>
class KSetting : public KAbstractSetting {
public:
    T value;
    KSetting(const std::string& name, const T& value) : KAbstractSetting(name), value(value) {
    }
    virtual bool load(std::istream& file) {
        std::string line;
        file.seekg(0, std::ios::beg);
        while (std::getline(file, line)) {
            if (line.substr(0, name.size()) == name) {
                file.seekg(- 1 - line.size() + name.size(), std::ios::cur);
                char swap;
                do {file.get(swap);} while(std::isspace(swap) && !file.eof() && swap != '\n');
                if (swap != ':') return false;
                do {file.get(swap);} while(std::isspace(swap) && !file.eof() && swap != '\n');
                file.putback(swap);
                file >> value;
                return true;
            }
        }
        return false;
    }
    virtual void save(std::ostream& file) {
        file.seekp(0, std::ios::end);
        file << name << " : " << value << std::endl;
    }
    virtual ~KSetting() {}
};

template<typename T>
class KSetting <T*> : public KAbstractSetting {
public:
    T* const pointer;
    KSetting(const std::string& name, T* pointer) : KAbstractSetting(name), pointer(pointer) {
    }
    virtual bool load(std::istream& file) {
        std::string line;
        file.seekg(0, std::ios::beg);
        while (std::getline(file, line)) {
            if (line.substr(0, name.size()) == name) {
                file.seekg(- 1 - line.size() + name.size(), std::ios::cur);
                char swap;
                do {file.get(swap);} while(std::isspace(swap) && !file.eof() && swap != '\n');
                if (swap != ':') return false;
                do {file.get(swap);} while(std::isspace(swap) && !file.eof() && swap != '\n');
                file.putback(swap);
                file >> *pointer;
                return true;
            }
        }
        return false;
    }
    virtual void save(std::ostream& file) {
        file.seekp(0, std::ios::end);
        file << name << " : " << *pointer << std::endl;
    }
    virtual ~KSetting() {}
};

class KSettingLoad {
    std::istream& mFile;
public:
    KSettingLoad(std::istream& file) : mFile(file) {}
    void operator()(KAbstractSetting& setting) {
        setting.load(mFile);
    }
    void operator()(KAbstractSetting* setting) {
        setting->load(mFile);
    }
};

class KSettingSave {
    std::ostream& mFile;
public:
    KSettingSave(std::ostream& file) : mFile(file) {}
    void operator()(KAbstractSetting& setting) {
        setting.save(mFile);
    }
    void operator()(KAbstractSetting* setting) {
        setting->save(mFile);
    }
};

#endif
