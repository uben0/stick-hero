#ifndef KBoard_HEADER_DEFINED
#define KBoard_HEADER_DEFINED

#include "KLib_SDL.hpp"
#include <cassert>
#include <string>
#include "KRect.hpp"
#include "KColor.hpp"
#include "KSurface.hpp"

class KBoard {
    static constexpr unsigned char type = 1;
    KLib_SDL      sdl_init;
    SDL_Window*   mWindow;
    SDL_Surface*  mSurface;
    void display();
public:
    const int w, h;
    const unsigned int id;
    KBoard();
    KBoard(int width, int height);
    ~KBoard();

    bool open(int width, int height);
    void close();
    void wait_closure();

    bool is_opened();
    bool is_closed();

    void fill(const KRect<>& rect, KColor color);
    void fill(                     KColor color);
    void print(const KSurface& src, int x, int y);
};

#endif
