#ifndef KLib_SDL_HEADER_DEFINED
#define KLib_SDL_HEADER_DEFINED

#include <SDL2/SDL.h>
#include <string>

class KLib_SDL {
    static int instances;
    void verbose(const std::string& message);
public:
    KLib_SDL();
    ~KLib_SDL();
};

#endif
