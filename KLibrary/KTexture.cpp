#include "KTexture.hpp"

 #include <cassert>

KTexture::KTexture() : mShared(nullptr), w(0), h(0) {
}

KTexture::KTexture(SDL_Texture* src, int w, int h) : KTexture() {
    assert(src != nullptr);
    mShared = new Shared;
    mShared->instances = 1;
    mShared->ptr = src;
    *((int*)&this->w) = w;
    *((int*)&this->h) = h;
}

KTexture::KTexture(const KTexture& src) : KTexture() {
    mShared = src.mShared;
    if (mShared != nullptr) {
        mShared->instances++;
    }
    *((int*)&w) = src.w;
    *((int*)&h) = src.h;
}

KTexture::~KTexture() {
    if (mShared != nullptr) {
        mShared->instances--;
        if (mShared->instances == 0) {
            SDL_DestroyTexture(mShared->ptr);
            delete mShared;
        }
    }
}

SDL_Texture* KTexture::get_ptr() const {
    assert(mShared != nullptr);
    return mShared->ptr;
}

bool KTexture::is_empty() const {
    return mShared == nullptr;
}

KTexture& KTexture::operator=(const KTexture& src) {
    if (mShared != nullptr) {
        mShared->instances--;
        if (mShared->instances == 0) {
            SDL_DestroyTexture(mShared->ptr);
            delete mShared;
        }
    }
    mShared = src.mShared;
    if (mShared != nullptr) {
        mShared->instances++;
    }
    *((int*)&w) = src.w;
    *((int*)&h) = src.h;
    return *this;
}
