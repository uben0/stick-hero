#ifndef KColor_HEADER_DEFINED
#define KColor_HEADER_DEFINED

#include <SDL2/SDL.h>
#include <iostream>

struct KColor {
    // 4 color canal : red, green, blue, alpha
    unsigned char r, g, b, a;

    KColor();
    // hexadecimal constructor : 0xffffffff
    KColor(unsigned long rgba);
    // 4 byte constructor : 255, 255, 255, 255
    KColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);
    // use it only for SDL functions
    SDL_Color to_SDL_Color() const;

    inline operator Uint32() const {
        return *(Uint32*)this;
    }
    static const KColor White;
    static const KColor Black;
    static const KColor Red;
    static const KColor Green;
    static const KColor Blue;
    static const KColor Cyan;
    static const KColor Magenta;
    static const KColor Yellow;
    static const KColor Transparent;
};

std::ostream& operator<<(std::ostream& ostream, const KColor& color);
std::istream& operator>>(std::istream& istream,       KColor& color);

#endif
