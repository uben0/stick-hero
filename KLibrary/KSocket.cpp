#include <iostream>
#include <cstring>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <bitset>

#include "KSocket.hpp"
#include "KTool.hpp"
#include "KTerm.hpp"

constexpr auto& socket_close   = close;
constexpr auto& socket_open    = socket;
constexpr auto& socket_listen  = listen;
constexpr auto& socket_accept  = accept;
constexpr auto& socket_connect = connect;
constexpr auto& socket_read    = read;
constexpr auto& socket_write   = write;
constexpr auto& socket_bind    = bind;

KSocket::Addr::Addr() {
    ss_family = IpvX;
}

void KSocket::Addr::clear() {
    ss_family = IpvX;
}

KSocket::Addr::Addr(const char * address, unsigned short port) {
    memset(this, 0, sizeof *this);
    ss_family = IpvX;
    if (address) {
        char buff[AddrLenght];
        if (address[0] == '[') {
            int i = 0;
            while (address[i] != '\0' && address[i] != ']') i++;
            if (address[i] == ']' && address[i + 1] == ':') {
                if (address[i + 2] < '0' || address[i + 2] > '9') {
                    return;
                }
                port = atoi(&address[i + 2]);
                strncpy(buff, address, sizeof buff);
                buff[i] = '\0';
                address = buff;
                address++;
            } else {
                strncpy(buff, address + 1, i - 1);
                if (inet_pton(Ipv6, buff, &((sockaddr_in6*)this)->sin6_addr) > 0) {
                    ((sockaddr_in6*)this)->sin6_port = htons(port);
                    ss_family = Ipv6;
                }
                return;
            }
        }
        if (inet_pton(Ipv4, address, &((sockaddr_in*)this)->sin_addr) > 0) {
            ((sockaddr_in*)this)->sin_port = htons(port);
            ss_family = Ipv4;
        } else
        if (inet_pton(Ipv6, address, &((sockaddr_in6*)this)->sin6_addr) > 0) {
            ((sockaddr_in6*)this)->sin6_port = htons(port);
            ss_family = Ipv6;
        } else {
            int i;
            for (i = 0; address[i] != '\0' && address[i] != ':'; i++);
            if (address[i] == ':') {
                if (address[i + 1] < '0' || address[i + 1] > '9') {
                    return;
                }
                port = atoi(&address[i + 1]);
                strncpy(buff, address, sizeof buff);
                buff[i] = '\0';
                address = buff;
                if (inet_pton(Ipv4, address, &((sockaddr_in*)this)->sin_addr) > 0) {
                    ((sockaddr_in*)this)->sin_port = htons(port);
                    ss_family = Ipv4;
                }
            }
        }
    }
}

KSocket::Addr::Addr(std::string const& address, unsigned short port) : Addr(address.c_str(), port) {
}

KSocket::Addr::Addr(sockaddr_storage const& address) {
    *((sockaddr_storage*)this) = address;
}

KSocket::Addr::Addr(const struct sockaddr * address) {
    if (address == nullptr) {
        ss_family = IpvX;
        return;
    }
    switch (address->sa_family) {
        case Ipv4:
        memcpy(this, address, sizeof(sockaddr_in));
        break;
        case Ipv6:
        memcpy(this, address, sizeof(sockaddr_in6));
    }
}

const sockaddr_storage * KSocket::Addr::get_struct_ptr() const {
    return this;
}

const sockaddr * KSocket::Addr::get_addr_ptr() const {
    return (const sockaddr*) this;
}

unsigned int KSocket::Addr::get_len() const {
    switch (ss_family) {
        case Ipv4: return sizeof (sockaddr_in);
        case Ipv6: return sizeof (sockaddr_in6);
    }
    return 0;
}

KSocket::IpVersion KSocket::Addr::get_ip_version() const {
    return ss_family;
}

unsigned short KSocket::Addr::get_port() const {
    switch (ss_family) {
        case Ipv4: return ntohs(((sockaddr_in *)this)->sin_port);
        case Ipv6: return ntohs(((sockaddr_in6*)this)->sin6_port);
    }
    return 0;
}

bool KSocket::Addr::is_set() const {
    return ss_family == Ipv4 || ss_family == Ipv6;
}

KSocket::Addr::operator std::string() const {
    return to_string();
}

std::string KSocket::Addr::to_string(bool port) const {
    if (is_set() == false) {
        return std::string();
    }
    char buffer[AddrLenght];
    if (ss_family == Ipv4) {
        if (
            inet_ntop(
                Ipv4, &((const sockaddr_in*)this)->sin_addr,
                buffer, sizeof buffer
            ) == nullptr
        ) {
            return std::string();
        }
        if (port) {
            int writeCursor = strlen(buffer);
            buffer[writeCursor++] = ':';
            KTool::itoa(
                ntohs(((sockaddr_in*)this)->sin_port),
                buffer + writeCursor,
                PortLenght
            );
            return buffer;
        }
        return buffer;
    }
    else if (ss_family == Ipv6) {
        if (
            inet_ntop(
                Ipv6, &((const sockaddr_in6*)this)->sin6_addr,
                buffer + (port ? 1:0), sizeof buffer
            ) == nullptr
        ) {
            return std::string();
        }
        if (port) {
            buffer[0] = '[';
            int writeCursor = strlen(buffer);
            buffer[writeCursor++] = ']';
            buffer[writeCursor++] = ':';
            KTool::itoa(
                ntohs(((sockaddr_in6*)this)->sin6_port),
                buffer + writeCursor,
                PortLenght
            );
            return buffer;
        }
        return buffer;
    }
    return std::string();
}

KSocket::Addr KSocket::get_bind_addr() const {
    return mBindAddr;
}

KSocket::Addr KSocket::get_peer_addr() const {
    return mPeerAddr;
}

KSocket::IpVersion KSocket::get_ip_version() const {
    if (mSocket != Closed) {
        return mBindAddr.get_ip_version();
    }
    return IpvX;
}

unsigned short KSocket::get_port() const {
    if (mSocket != Closed) {
        return mBindAddr.get_port();
    }
    return 0;
}

void KSocket::verbose(std::string const& str) const {
    if (mPropertie & Verbose)
    std::cout << KTerm::Bold << "[" << KTerm::Color<120> << "KSocket"
    << KTerm::UColor << "] " << KTerm::UBold << KTerm::Color<120>
    << str << KTerm::Def << std::endl;
}

void KSocket::verbose(std::string const& str, int state) const {
    if (mPropertie & Verbose)
    std::cout << KTerm::Bold << "[" << KTerm::Color<120> << "KSocket"
    << KTerm::UColor << "] " << KTerm::UBold << KTerm::Color<120>
    << str << state << KTerm::Def << std::endl;
}

std::ostream& operator<<(std::ostream &ostream, KSocket::Addr const& addr) {
    return ostream << addr.to_string(true);
}

std::istream& operator>>(std::istream &istream, KSocket::Addr & addr) {
    char buff[INET6_ADDRSTRLEN + KSocket::PortLenght + 2];
    for (unsigned int i = 0; i < sizeof buff; i++) {
        istream.get(buff[i]);
        if (
            (buff[i] < '0' || buff[i] > '9') &&
            (buff[i] < 'a' || buff[i] > 'z') &&
            (buff[i] < 'A' || buff[i] > 'Z') &&
            buff[i] != ':' && buff[i] != '.' &&
            buff[i] != '[' && buff[i] != ']'
        ) {
            istream.putback(buff[i]);
            buff[i] = '\0';
            addr = buff;
            break;
        }
    }
    return istream;
}

bool KSocket::print_interface_address() {
    struct ifaddrs *ifaddrs, *ifaddr;
    std::cout << KTerm::Bold << "Name   Scope  Address"
    << KTerm::Dim << "/mask" << KTerm::Def << std::endl;
    if (getifaddrs(&ifaddrs) == -1) {
        return false;
    }
    for (
        ifaddr = ifaddrs;
        ifaddr != nullptr;
        ifaddr = ifaddr->ifa_next
    ) {
        if (ifaddr->ifa_addr->sa_family == Ipv4) {
            // print address
            if (
                ifaddr->ifa_name[0] == 'l' &&
                ifaddr->ifa_name[1] == 'o' &&
                ifaddr->ifa_name[2] == '\0'
            ) {
                std::cout << KTerm::Dim << "lo    " << KTerm::UDim;
            } else {
                std::cout << KTerm::Dim << ifaddr->ifa_name << KTerm::UDim;
            }
            std::cout << "        " << Addr(ifaddr->ifa_addr).to_string(false);
            // add mask value
            int tmpAddr = 0;
            memcpy(&tmpAddr, &((sockaddr_in*)ifaddr->ifa_netmask)->sin_addr, 4);
            auto bitset = std::bitset<32>(ntohl(tmpAddr));
            int i = 0;
            while (i < 32 && !bitset[i]) i++;
            std::cout << KTerm::Dim << "/" << i << KTerm::UDim << std::endl;
        } else
        if (ifaddr->ifa_addr->sa_family == Ipv6) {
            if (
                ifaddr->ifa_name[0] == 'l' &&
                ifaddr->ifa_name[1] == 'o' &&
                ifaddr->ifa_name[2] == '\0'
            ) {
                std::cout << KTerm::Dim << "lo     Host   " << KTerm::UDim;
                std::cout << Addr(ifaddr->ifa_addr).to_string(false);
                std::cout << KTerm::Dim << "/128" << KTerm::UDim << std::endl;
            } else {
                std::cout << KTerm::Dim << ifaddr->ifa_name;
                switch (((sockaddr_in6*)ifaddr->ifa_addr)->sin6_scope_id) {
                    case 0: std::cout << " Global " << KTerm::UDim; break;
                    case 2: std::cout << " Link   " << KTerm::UDim; break;
                }
                std::cout << Addr(ifaddr->ifa_addr).to_string(false);
                std::cout << KTerm::Dim << "/64" << KTerm::UDim << std::endl;
            }
        }
    }
    freeifaddrs(ifaddrs);
    return true;
}

KSocket::KSocket(Propertie propertie) :
    mSocket(Closed),
    mBindAddr(),
    mPeerAddr(),
    mPropertie(propertie)
{}

void KSocket::reset(Propertie propertie) {
    mSocket = Closed;
    mBindAddr.clear();
    mPeerAddr.clear();
    mPropertie = propertie;
}

bool KSocket::is_opened() const {
    return mSocket != Closed;
}

bool KSocket::is_closed() const {
    return mSocket == Closed;
}

bool KSocket::bind(const Addr& address) {
    if (mSocket == Closed) {
        verbose("bind() exit : socket is not opened");
        return false;
    }
    if (mBindAddr.is_set()) {
        verbose("bind() exit : socket is already bound");
        return false;
    }
    if (address.is_set() == false) {
        verbose("bind() exit : provided address is empty");
        return false;
    }
    if (socket_bind(mSocket, (sockaddr*)&address, sizeof address) == -1) {
        verbose("bind() failed :");
        perror(nullptr);
        return false;
    }
    socklen_t tmpAddrLen = sizeof mBindAddr;
    if (getsockname(mSocket, (sockaddr*)&mBindAddr, &tmpAddrLen) == -1) {
        verbose("getsockname() failed");
        perror(nullptr);
        return false;
    }
    verbose("bound to " + mBindAddr.to_string());
    return true;
}

bool KSocket::set_propertie(Propertie propertie) {
    bool success = true;
    if (mSocket != Closed) {
        if ((propertie & Broadcast) != (mPropertie & Broadcast)) {
            success &= apply_propertie_broadcast(propertie & Broadcast);
        }
        if ((propertie & ReuseAddr) != (mPropertie & ReuseAddr)) {
            success &= apply_propertie_reuse_addr(propertie & ReuseAddr);
        }
        if ((propertie & Blocking) != (mPropertie & Blocking)) {
            success &= apply_propertie_blocking(propertie & Blocking);
        }
    }
    mPropertie = propertie;
    return success;
}

bool KSocket::set_propertie(bool set, Propertie propertie) {
    propertie = set ? (mPropertie | propertie) : (mPropertie & ~propertie);
    return set_propertie(propertie);
}

bool KSocket::init_propertie() {
    if (mSocket == Closed) {
        verbose("init_propertie() exit : socket is not opened");
        return false;
    }
    bool success = true;
    if (mPropertie & Broadcast) {
        success &= apply_propertie_broadcast(true);
    }
    if (mPropertie & ReuseAddr) {
        success &= apply_propertie_reuse_addr(true);
    }
    if ((mPropertie & Blocking) == false) {
        success &= apply_propertie_blocking(false);
    }
    return success;
}

bool KSocket::apply_propertie_blocking(bool set) {
    int flags = fcntl(mSocket, F_GETFL, 0);
    if (flags == -1) {
        perror("set blocking failed");
        return false;
    }
    flags = set ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
    if (fcntl(mSocket, F_SETFL, flags)) {
        perror("set blocking failed");
        return false;
    }
    verbose("blocking propertie applied : ", int(set));
    return true;
}

bool KSocket::apply_propertie_broadcast(bool set) {
    int boolean = set;
    if (setsockopt(mSocket, SOL_SOCKET, SO_BROADCAST, &boolean, sizeof boolean) == -1) {
        perror("unable to set broadcast propertie");
        return false;
    }
    verbose("broadcast propertie applied : ", int(set));
    return true;
}

bool KSocket::apply_propertie_reuse_addr(bool set) {
    int boolean = set;
    if (setsockopt(mSocket, SOL_SOCKET, SO_REUSEADDR, &boolean, sizeof boolean) == -1) {
        perror("unable to set reuse address propertie");
        return false;
    }
    verbose("reuse address propertie applied : ", int(set));
    return true;
}

bool KSocket::join_membership(const Addr& address) {
    if (mSocket == Closed) {
        verbose("join_membership() exit : socket is not opened");
        return false;
    }
    if (address.get_ip_version() != Ipv6) {
        verbose("join_membership() exit : address is not ipv6");
        return false;
    }
    struct ipv6_mreq group = {((sockaddr_in6*)&address)->sin6_addr, 0};
    if(setsockopt(mSocket, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &group, sizeof group) == -1) {
        perror("unable to join membership");
        return false;
    }
    verbose("membership " + address.to_string(false) + " joined");
    return true;
}

bool KSocket::drop_membership(const Addr& address) {
    if (mSocket == Closed) {
        verbose("drop_membership() exit : socket is not opened");
        return false;
    }
    if (address.get_ip_version() != Ipv6) {
        verbose("drop_membership() exit : address is not ipv6");
        return false;
    }
    struct ipv6_mreq group = {((sockaddr_in6*)&address)->sin6_addr, 0};
    if(setsockopt(mSocket, IPPROTO_IPV6, IPV6_DROP_MEMBERSHIP, &group, sizeof group) == -1) {
        perror("unable to drop membership");
        return false;
    }
    verbose("membership " + address.to_string(false) + " droped");
    return true;
}
