#include "KKey.hpp"

std::ostream& operator<<(std::ostream& ostream, const KKey::Id& key) {
    switch (key) {
        case KKey::Digit0:
        ostream << "Digit0";
        break;
        case KKey::Digit1:
        ostream << "Digit1";
        break;
        case KKey::Digit2:
        ostream << "Digit2";
        break;
        case KKey::Digit3:
        ostream << "Digit3";
        break;
        case KKey::Digit4:
        ostream << "Digit4";
        break;
        case KKey::Digit5:
        ostream << "Digit5";
        break;
        case KKey::Digit6:
        ostream << "Digit6";
        break;
        case KKey::Digit7:
        ostream << "Digit7";
        break;
        case KKey::Digit8:
        ostream << "Digit8";
        break;
        case KKey::Digit9:
        ostream << "Digit9";
        break;
        case KKey::A:
        ostream << "A";
        break;
        case KKey::B:
        ostream << "B";
        break;
        case KKey::C:
        ostream << "C";
        break;
        case KKey::D:
        ostream << "D";
        break;
        case KKey::E:
        ostream << "E";
        break;
        case KKey::F:
        ostream << "F";
        break;
        case KKey::G:
        ostream << "G";
        break;
        case KKey::H:
        ostream << "H";
        break;
        case KKey::I:
        ostream << "I";
        break;
        case KKey::J:
        ostream << "J";
        break;
        case KKey::K:
        ostream << "K";
        break;
        case KKey::L:
        ostream << "L";
        break;
        case KKey::M:
        ostream << "M";
        break;
        case KKey::N:
        ostream << "N";
        break;
        case KKey::O:
        ostream << "O";
        break;
        case KKey::P:
        ostream << "P";
        break;
        case KKey::Q:
        ostream << "Q";
        break;
        case KKey::R:
        ostream << "R";
        break;
        case KKey::S:
        ostream << "S";
        break;
        case KKey::T:
        ostream << "T";
        break;
        case KKey::U:
        ostream << "U";
        break;
        case KKey::V:
        ostream << "V";
        break;
        case KKey::W:
        ostream << "W";
        break;
        case KKey::X:
        ostream << "X";
        break;
        case KKey::Y:
        ostream << "Y";
        break;
        case KKey::Z:
        ostream << "Z";
        break;
        case KKey::Return:
        ostream << "Return";
        break;
        case KKey::Left:
        ostream << "Left";
        break;
        case KKey::Right:
        ostream << "Right";
        break;
        case KKey::Up:
        ostream << "Up";
        break;
        case KKey::Down:
        ostream << "Down";
        break;
        case KKey::LShift:
        ostream << "LShift";
        break;
        case KKey::RShift:
        ostream << "RShift";
        break;
        case KKey::LCtrl:
        ostream << "LCtrl";
        break;
        case KKey::RCtrl:
        ostream << "RCtrl";
        break;
        case KKey::LAlt:
        ostream << "LAlt";
        break;
        case KKey::RAlt:
        ostream << "RAlt";
        break;
        case KKey::Space:
        ostream << "Space";
        break;
        case KKey::Tab:
        ostream << "Tab";
        break;
        case KKey::Escape:
        ostream << "Escape";
        break;
    }
    return ostream;
}

std::istream& operator>>(std::istream& istream, KKey::Id& key) {
    std::string buffer;
    istream >> buffer;
    if (buffer.size() == 1) {
        switch (buffer[0]) {
            case 'A':
            key = KKey::A;
            break;
            case 'B':
            key = KKey::B;
            break;
            case 'C':
            key = KKey::C;
            break;
            case 'D':
            key = KKey::D;
            break;
            case 'E':
            key = KKey::E;
            break;
            case 'F':
            key = KKey::F;
            break;
            case 'G':
            key = KKey::G;
            break;
            case 'H':
            key = KKey::H;
            break;
            case 'I':
            key = KKey::I;
            break;
            case 'J':
            key = KKey::J;
            break;
            case 'K':
            key = KKey::K;
            break;
            case 'L':
            key = KKey::L;
            break;
            case 'M':
            key = KKey::M;
            break;
            case 'N':
            key = KKey::N;
            break;
            case 'O':
            key = KKey::O;
            break;
            case 'P':
            key = KKey::P;
            break;
            case 'Q':
            key = KKey::Q;
            break;
            case 'R':
            key = KKey::R;
            break;
            case 'S':
            key = KKey::S;
            break;
            case 'T':
            key = KKey::T;
            break;
            case 'U':
            key = KKey::U;
            break;
            case 'V':
            key = KKey::V;
            break;
            case 'W':
            key = KKey::W;
            break;
            case 'X':
            key = KKey::X;
            break;
            case 'Y':
            key = KKey::Y;
            break;
            case 'Z':
            key = KKey::Z;
            break;
        }
    } else
    if (buffer[0] == 'D' && buffer.size() == 6) {
        switch (buffer[5]) {
            case '0':
            key = KKey::Digit0;
            break;
            case '1':
            key = KKey::Digit1;
            break;
            case '2':
            key = KKey::Digit2;
            break;
            case '3':
            key = KKey::Digit3;
            break;
            case '4':
            key = KKey::Digit4;
            break;
            case '5':
            key = KKey::Digit5;
            break;
            case '6':
            key = KKey::Digit6;
            break;
            case '7':
            key = KKey::Digit7;
            break;
            case '8':
            key = KKey::Digit8;
            break;
            case '9':
            key = KKey::Digit9;
            break;
        }
    } else
    if (buffer[0] == 'L') {
        switch (buffer[1]) {
            case 'e':
            key = KKey::Left;
            break;
            case 'S':
            key = KKey::LShift;
            break;
            case 'C':
            key = KKey::LCtrl;
            break;
            case 'A':
            key = KKey::LAlt;
        }
    } else
    if (buffer[0] == 'R') {
        switch (buffer[1]) {
            case 'i':
            key = KKey::Right;
            break;
            case 'S':
            key = KKey::RShift;
            break;
            case 'C':
            key = KKey::RCtrl;
            break;
            case 'A':
            key = KKey::RAlt;
        }
    } else {
        if (buffer == "Up") {
            key = KKey::Up;
        } else
        if (buffer == "Down") {
            key = KKey::Down;
        } else
        if (buffer == "Space") {
            key = KKey::Space;
        } else
        if (buffer == "Return") {
            key = KKey::Return;
        } else
        if (buffer == "Escape") {
            key = KKey::Escape;
        } else
        if (buffer == "Tab") {
            key = KKey::Tab;
        }
    }
    return istream;
}
