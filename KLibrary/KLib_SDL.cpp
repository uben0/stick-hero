#include <iostream>
#include <cassert>
#include "KTerm.hpp"
#include "KLib_SDL.hpp"

int KLib_SDL::instances = 0;

void KLib_SDL::verbose(const std::string& message) {
    std::cout << KTerm::Bold << "[" << KTerm::Color<117> << "KLib_SDL"
    << KTerm::UColor << "]" << KTerm::UBold << " " << KTerm::Color<117> << message
    << KTerm::Def << std::endl;
}

KLib_SDL::KLib_SDL() {
    if (instances == 0) {
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            verbose("enable to initialize");
            exit(-1);
        }
        verbose("initialized");
    }
    instances++;
}

KLib_SDL::~KLib_SDL() {
    instances--;
    if (instances == 0) {
        SDL_Quit();
        verbose("quit");
    }
}
