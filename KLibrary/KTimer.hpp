#ifndef KTimer_HEADER_DEFINED
#define KTimer_HEADER_DEFINED

class KTimer {
    unsigned int mMsPerLoop;
    unsigned long long mCount;
    unsigned long long mStart;
public:
    static constexpr unsigned int Fps60 = 1000/60;
    static constexpr unsigned int Fps30 = 1000/30;
    const unsigned int& ms    = mMsPerLoop;
    const unsigned int& count = mCount;
    KTimer(unsigned int msPerLoop);
    void start(unsigned int msPerLoop);
    void start();
    void wait();
    unsigned long long get_time() const;
    static void wait_for(unsigned int msToWait);
};

#endif
