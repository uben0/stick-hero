#ifndef KControl_HEADER_DEFINED
#define KControl_HEADER_DEFINED

#include "KKey.hpp"

class KControl {
    const unsigned char * const key;
public:
    struct {
        int x, y;
        bool l, m, r;
    } mouse;

    KControl();
    void update();
    bool operator[](KKey::Id keyId) const;
};

#endif
