#ifndef KKey_HEADER_DEFINED
#define KKey_HEADER_DEFINED

#include <iostream>
#include "KLib_SDL.hpp"

namespace KKey {
    constexpr int Num = SDL_NUM_SCANCODES;
    enum Id {
        Digit0 = SDLK_0,
        Digit1 = SDLK_1,
        Digit2 = SDLK_2,
        Digit3 = SDLK_3,
        Digit4 = SDLK_4,
        Digit5 = SDLK_5,
        Digit6 = SDLK_6,
        Digit7 = SDLK_7,
        Digit8 = SDLK_8,
        Digit9 = SDLK_9,
        A = SDLK_a,
        B = SDLK_b,
        C = SDLK_c,
        D = SDLK_d,
        E = SDLK_e,
        F = SDLK_f,
        G = SDLK_g,
        H = SDLK_h,
        I = SDLK_i,
        J = SDLK_j,
        K = SDLK_k,
        L = SDLK_l,
        M = SDLK_m,
        N = SDLK_n,
        O = SDLK_o,
        P = SDLK_p,
        Q = SDLK_q,
        R = SDLK_r,
        S = SDLK_s,
        T = SDLK_t,
        U = SDLK_u,
        V = SDLK_v,
        W = SDLK_w,
        X = SDLK_x,
        Y = SDLK_y,
        Z = SDLK_z,
        Return = SDLK_RETURN,
        Left   = SDLK_LEFT,
        Right  = SDLK_RIGHT,
        Up     = SDLK_UP,
        Down   = SDLK_DOWN,
        LShift = SDLK_LSHIFT,
        RShift = SDLK_RSHIFT,
        LCtrl  = SDLK_LCTRL,
        RCtrl  = SDLK_RCTRL,
        LAlt   = SDLK_LALT,
        RAlt   = SDLK_RALT,
        Space  = SDLK_SPACE,
        Tab    = SDLK_TAB,
        Escape = SDLK_ESCAPE
    };
}

std::ostream& operator<<(std::ostream& ostream, const KKey::Id& key);
std::istream& operator>>(std::istream& istream,       KKey::Id& key);

#endif
