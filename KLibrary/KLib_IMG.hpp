#ifndef KLib_IMG_HEADER_DEFINED
#define KLib_IMG_HEADER_DEFINED

#include <SDL2/SDL_image.h>
#include <string>

class KLib_IMG {
    static int instances;
    void verbose(const std::string& message);
public:
    KLib_IMG();
    ~KLib_IMG();
};

#endif
