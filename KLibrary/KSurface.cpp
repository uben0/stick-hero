#include "KSurface.hpp"
#include "KBoard.hpp"
#include <cassert>

KSurface::KSurface() : mSurface(nullptr), w(0), h(0) {
}

KSurface::KSurface(SDL_Surface* src) : mSurface(src), w(src ? src->w : 0), h(src ? src->h : 0) {
    if (mSurface != nullptr) {
        mSurface->userdata = (void*)1;
    }
}

KSurface::KSurface(const KSurface& src) : KSurface(src.mSurface) {
    if (mSurface != nullptr) {
        mSurface->refcount++;
        mSurface->userdata = (void*)((char*)mSurface->userdata + 1);
    }
}

KSurface::KSurface(const std::string& fileName) : KSurface() {
    SDL_Surface* src = IMG_Load(fileName.c_str());
    mSurface = SDL_CreateRGBSurface(
        0, src->w, src->h, 32,
        KColor{255, 0, 0, 0},
        KColor{0, 255, 0, 0},
        KColor{0, 0, 255, 0},
        KColor{0, 0, 0, 255}
    );
    SDL_BlitSurface(src, nullptr, mSurface, nullptr);
    SDL_FreeSurface(src);
    *((int*)&w) = mSurface->w;
    *((int*)&h) = mSurface->h;
    mSurface->userdata = (void*)1;
}

KSurface::KSurface(int width, int height) : KSurface(
    SDL_CreateRGBSurface(
        0, width, height, 32,
        KColor{255, 0, 0, 0},
        KColor{0, 255, 0, 0},
        KColor{0, 0, 255, 0},
        KColor{0, 0, 0, 255}
    )
) {}

KSurface::KSurface(int width, int height, KColor color) : KSurface(width, height) {
    fill(color);
}

KSurface::~KSurface() {
    mSurface->userdata = (void*)((char*)mSurface->userdata - 1);
    SDL_FreeSurface(mSurface);
}

void KSurface::unlock_write() {
    if (mSurface->userdata > (void*)1) {
        SDL_Surface* duplicated = SDL_CreateRGBSurface(
            0, w, h, 32,
            KColor{255, 0, 0, 0},
            KColor{0, 255, 0, 0},
            KColor{0, 0, 255, 0},
            KColor{0, 0, 0, 255}
        );
        SDL_BlitSurface(mSurface, nullptr, duplicated, nullptr);
        mSurface->userdata = (void*)((char*)mSurface->userdata - 1);
        mSurface->refcount--;
        mSurface = duplicated;
        mSurface->userdata = (void*)1;
    }
}

bool KSurface::is_empty() const {
    return mSurface == nullptr;
}

SDL_Surface* KSurface::get_ptr() const {
    return mSurface;
}

KSurface& KSurface::operator=(const KSurface& src) {
    if (src.mSurface != nullptr) {
        src.mSurface->userdata = (void*)((char*)src.mSurface->userdata + 1);
        src.mSurface->refcount++;
    }
    mSurface->userdata = (void*)((char*)mSurface->userdata - 1);
    SDL_FreeSurface(mSurface);
    mSurface = src.mSurface;
    *((int*)&w) = mSurface->w;
    *((int*)&h) = mSurface->h;
    return *this;
}

void KSurface::fill(const KRect<>& rect, KColor color) {
    assert(mSurface != nullptr);
    unlock_write();
    SDL_Rect sdlRect = {rect.x, h - rect.y - rect.h, rect.w, rect.h};
    SDL_FillRect(mSurface, &sdlRect, SDL_MapRGBA(mSurface->format, color.r, color.g, color.b, color.a));
}

void KSurface::fill(KColor color) {
    assert(mSurface != nullptr);
    unlock_write();
    SDL_FillRect(mSurface, nullptr, SDL_MapRGBA(mSurface->format, color.r, color.g, color.b, color.a));
}

void KSurface::print(const KSurface& src, int x, int y) {
    assert(mSurface != nullptr && src.mSurface != nullptr);
    unlock_write();
    SDL_Rect sdlRect = {x, h - y - src.h, 0, 0};
    SDL_BlitSurface(src.mSurface, nullptr, mSurface, &sdlRect);
}

KColor KSurface::get_pixel(int x, int y) const {
    // assert(mSurface != nullptr);
    // assert(x >= 0 && x <= w && y >= 0 && y <= h);
    return ((KColor*)mSurface->pixels)[(h - y - 1) * w + x];
    // Uint32 pixel = *((Uint32*)((Uint8*)mSurface->pixels + (h - y - 1) * mSurface->pitch + x * 4));
    // return *(KColor*)&pixel;
}

void KSurface::set_pixel(int x, int y, KColor color) {
    // assert(mSurface != nullptr);
    // assert(x >= 0 && x <= w && y >= 0 && y <= h);
    if (mSurface->userdata > (void*)1) {
        unlock_write();
    }
    ((KColor*)mSurface->pixels)[(h - y - 1) * w + x] = color;
    // *((Uint32*)((Uint8*)mSurface->pixels + (h - y - 1) * mSurface->pitch + x * 4)) = color;
}

void KSurface::show() const {
    KBoard board(w, h);
    board.print(*this, 0, 0);
    board.wait_closure();
}

void KSurface::save(const std::string& fileName) const {
    int end = fileName.size();
    if (
        fileName[end - 4] == '.' &&
        fileName[end - 3] == 'b' &&
        fileName[end - 2] == 'm' &&
        fileName[end - 1] == 'p'
    ) {
        SDL_SaveBMP(mSurface, fileName.c_str());
    } else {
        SDL_SaveBMP(mSurface, (fileName + ".bmp").c_str());
    }
}

KSurface KSurface::smaller_copy(int factor) const {
    KSurface smallerDst(w / factor, h / factor);
    for (int x = 0; x < smallerDst.w; x++) {
        for (int y = 0; y < smallerDst.h; y++) {
            smallerDst.set_pixel(x, y, get_pixel(x * factor, y * factor));
        }
    }
    return smallerDst;
}

KSurface KSurface::bigger_copy(int factor) const {
    KSurface dst(w * factor, h * factor);
    for (int x = 0; x < w; x++) {
        for (int y = 0; y < h; y++) {
            for (int xB = 0; xB < factor; xB++) {
                for (int yB = 0; yB < factor; yB++) {
                    dst.set_pixel(x * factor + xB, y * factor + yB, get_pixel(x, y));
                }
            }
        }
    }
    return dst;
}

void KSurface::blur(int radius) {
    KColor* src = get_map();
    KColor* dst = new KColor[w * h];
    int surface = radius * 2;
    KColor color;
    for (int y = 0; y < h; y++) {
        int r = 0, g = 0, b = 0;
        color = src[y * w];
        r += color.r * radius;
        g += color.g * radius;
        b += color.b * radius;
        for (int x = 0; x < radius; x++) {
            color = src[y * w + ((x >= w) ? (w - 1) : x)];
            r += color.r;
            g += color.g;
            b += color.b;
        }
        for (int x = 0; x < w; x++) {
            dst[y * w + x].r = r / surface;
            dst[y * w + x].g = g / surface;
            dst[y * w + x].b = b / surface;
            color = src[y * w + ((x + radius >= w) ? (w - 1) : (x + radius))];
            r += color.r;
            g += color.g;
            b += color.b;
            color = src[y * w + ((x - radius < 0) ? 0 : (x - radius))];
            r -= color.r;
            g -= color.g;
            b -= color.b;
        }
    }
    std::swap(src, dst);
    for (int x = 0; x < w; x++) {
        int r = 0, g = 0, b = 0;
        color = src[x];
        r += color.r * radius;
        g += color.g * radius;
        b += color.b * radius;
        for (int y = 0; y < radius; y++) {
            color = src[((y >= h) ? (h - 1) : y) * w + x];
            r += color.r;
            g += color.g;
            b += color.b;
        }
        for (int y = 0; y < h; y++) {
            dst[y * w + x].r = r / surface;
            dst[y * w + x].g = g / surface;
            dst[y * w + x].b = b / surface;
            color = src[((y + radius >= h) ? (h - 1) : (y + radius)) * w + x];
            r += color.r;
            g += color.g;
            b += color.b;
            color = src[((y - radius < 0) ? 0 : (y - radius)) * w + x];
            r -= color.r;
            g -= color.g;
            b -= color.b;
        }
    }
    delete src;
}

void KSurface::blur_strong(int factor) {
    assert(factor > 0 && factor <= 16);
    if (factor > 1) {
        *this = smaller_copy(16);
        blur(2 * factor);
        *this = bigger_copy(4);
        blur(3 * factor);
        *this = bigger_copy(2);
        blur(factor);
        *this = bigger_copy(2);
    } else {
        *this = smaller_copy(8);
        blur(2 * factor);
        *this = bigger_copy(2);
        blur(3 * factor);
        *this = bigger_copy(2);
        blur(2 * factor);
        *this = bigger_copy(2);
    }
}

void KSurface::darker(unsigned char factor) {
    KColor* map = get_map();
    for (int i = 0; i < w * h; i++) {
        map[i].r = map[i].r * factor / 255;
        map[i].g = map[i].g * factor / 255;
        map[i].b = map[i].b * factor / 255;
    }
}
