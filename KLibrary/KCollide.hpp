#ifndef KCollide_HEADER_DEFINED
#define KCollide_HEADER_DEFINED

#include "KRect.hpp"

namespace KCollide {
    enum Direction {
        Up, Down, Right, Left
    };
    template <typename T = int, typename U = T>
    struct Border {
        Direction direction;
        T x, y;
        U length;
        int id;
    };
    template<typename T = int, typename U = T>
    double up(const KRect<T, U>& rect, T vx, T vy, const Border<T, U>& border) {
        if (rect.y + rect.h <= border.y && rect.y + rect.h + vy > border.y) {
            vx *= (border.y - rect.y - rect.h) / vy;
            if (rect.x + vx + rect.w > border.x && rect.x + vx < border.x + border.length) {
                return (border.y - rect.y - rect.h) / vy;
            }
        }
        return 1.0;
    }
    template<typename T = int, typename U = T>
    double down(const KRect<T, U>& rect, T vx, T vy, const Border<T, U>& border) {
        if (rect.y >= border.y && rect.y + vy < border.y) {
            vx *= (rect.y - border.y) / vy;
            if (rect.x + vx + rect.w > border.x && rect.x + vx < border.x + border.length) {
                return (rect.y - border.y) / vy;
            }
        }
        return 1.0;
    }
    template<typename T = int, typename U = T>
    double right(const KRect<T, U>& rect, T vx, T vy, const Border<T, U>& border) {
        if (rect.x + rect.w <= border.x && rect.x + rect.w + vx > border.x) {
            vy *= (border.x - rect.x - rect.w) / vx;
            if (rect.y + vy + rect.h > border.y && rect.y + vy < border.y + border.length) {
                return (border.x - rect.x - rect.w) / vx;
            }
        }
        return 1.0;
    }
    template<typename T = int, typename U = T>
    double left(const KRect<T, U>& rect, T vx, T vy, const Border<T, U>& border) {
        if (rect.x >= border.x && rect.x + vx < border.x) {
            vy *= (rect.x - border.x) / vx;
            if (rect.y + vy + rect.h > border.y && rect.y + vy < border.y + border.length) {
                return (rect.x - border.x) / vx;
            }
        }
        return 1.0;
    }
    template<typename T = int, typename U = T, typename Iter>
    double find(const KRect<T, U>& rect, T vx, T vy, Iter start, Iter end, Iter& selected) {
        double best = 1;
        register double tmp;
        selected = end;

        // for optimization, sort all of 3*3 cases (go up, static or down, times, go left, static or right)
        if (vx > 0) {
            if (vy > 0) {
                // right and up
                while (start != end) {
                    if (start->direction == Right) {
                        tmp = right(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    } else
                    if (start->direction == Up) {
                        tmp = up(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            } else
            if (vy < 0) {
                // right and down
                while (start != end) {
                    if (start->direction == Right) {
                        tmp = right(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    } else
                    if (start->direction == Down) {
                        tmp = down(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            }
            else {
                // right and static
                while (start != end) {
                    if (start->direction == Right) {
                        tmp = right(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            }

        } else
        if (vx < 0) {
            if (vy > 0) {
                // left and up
                while (start != end) {
                    if (start->direction == Left) {
                        tmp = left(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    } else
                    if (start->direction == Up) {
                        tmp = up(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            } else
            if (vy < 0) {
                // left and down
                while (start != end) {
                    if (start->direction == Left) {
                        tmp = left(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    } else
                    if (start->direction == Down) {
                        tmp = down(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            }
            else {
                // left and static
                while (start != end) {
                    if (start->direction == Left) {
                        tmp = left(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            }
        }
        else {
            if (vy > 0) {
                // static and up
                while (start != end) {
                    if (start->direction == Up) {
                        tmp = up(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            } else
            if (vy < 0) {
                // static and down
                while (start != end) {
                    if (start->direction == Down) {
                        tmp = down(rect, vx, vy, *start);
                        if (tmp < best) {
                            best = tmp;
                            selected = start;
                        }
                    }
                    start++;
                }
            }
            // static total have no collision possible
        }
        return best;
    }
}

#endif
