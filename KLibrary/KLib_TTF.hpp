#ifndef KLib_TTF_HEADER_DEFINED
#define KLib_TTF_HEADER_DEFINED

#include <SDL2/SDL_ttf.h>
#include <string>

class KLib_TTF {
    static int instances;
    void verbose(const std::string& message);
public:
    KLib_TTF();
    ~KLib_TTF();
};

#endif
