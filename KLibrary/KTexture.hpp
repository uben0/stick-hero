#ifndef KTexture_HEADER_DEFINED
#define KTexture_HEADER_DEFINED

#include "KLib_IMG.hpp"

class KTexture {
    KLib_IMG img_init;
    struct Shared {
        int instances;
        SDL_Texture* ptr;
    } *mShared;
public:
    const int w, h;
    KTexture();
    KTexture(SDL_Texture* src, int w, int h);
    KTexture(const KTexture& src);
    ~KTexture();
    bool is_empty() const;
    KTexture& operator=(const KTexture& src);
    SDL_Texture* get_ptr() const;
};

#endif
