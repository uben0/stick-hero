#include <iostream>
#include <cassert>
#include "KTerm.hpp"
#include "KLib_TTF.hpp"

int KLib_TTF::instances = 0;

void KLib_TTF::verbose(const std::string& message) {
    std::cout << KTerm::Bold << "[" << KTerm::Color<117> << "KLib_TTF"
    << KTerm::UColor << "]" << KTerm::UBold << " " << KTerm::Color<117> << message
    << KTerm::Def << std::endl;
}

KLib_TTF::KLib_TTF() {
    if (instances == 0) {
        if (TTF_Init() != 0) {
            verbose("enable to initialize");
            exit(-1);
        }
        verbose("initialized");
    }
    instances++;
}

KLib_TTF::~KLib_TTF() {
    instances--;
    if (instances == 0) {
        TTF_Quit();
        verbose("quit");
    }
}
