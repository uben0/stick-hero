#ifndef KFont_HEADER_DEFINED
#define KFont_HEADER_DEFINED

#include <string>
#include "KLib_TTF.hpp"
#include "KSurface.hpp"
#include "KColor.hpp"

class KFont {
    KLib_TTF ttf_init;
    TTF_Font* mFont;
public:
    KFont();
    KFont(const std::string& file, int size);
    ~KFont();
    bool is_opened();
    KSurface operator()(const std::string& text, KColor color);
};

#endif
