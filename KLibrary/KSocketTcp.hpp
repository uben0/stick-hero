#ifndef KSocketTcp_HEADER_DEFINED
#define KSocketTcp_HEADER_DEFINED

#include "KSocket.hpp"

class KSocketTcp : public KSocket {
    virtual void abstract() {return;};
    bool mListenning;
    int  mInQueue;
    void reset(Propertie propertie);
public:
    KSocketTcp(Propertie propertie = 0);
    KSocketTcp(const Addr& address, int queueLen, Propertie propertie = 0);
    ~KSocketTcp();
    bool open(IpVersion ipVersion = IpvX);
    bool open(const Addr& address);// +bind(address)
    bool open(const Addr& address, int queueLen);// +bind(address) +listen(queueLen)
    void close();
    bool listen(int queueLen);
    int  in_queue();
    bool accept(KSocketTcp& client) const;
    bool connect(const Addr& address);
    int  read(       void* buffer, int size);
    int  write(const void* buffer, int size);
    int  write(const char* message);
};

std::ostream& operator<<(std::ostream &ostream, KSocketTcp& socket);
std::istream& operator>>(std::istream &istream, KSocketTcp& socket);

#endif
