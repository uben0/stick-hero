#ifndef KRect_HEADER_DEFINED
#define KRect_HEADER_DEFINED

template<typename T = int, typename U = T>
struct KRect {
    T x, y;
    U w, h;
    KRect(): x(0), y(0), w(0), h(0) {}
    template<typename V, typename W, typename X, typename Y>
    KRect(V x, W y, X w, Y h): x(x), y(y), w(w), h(h) {}
    template<typename V, typename W>
    KRect(const KRect<V, W>& src): x(src.x), y(src.y), w(src.w), h(src.h) {}
    bool is_on(T x_, T y_) {
        return x_ >= x && x_ < x + w && y_ >= y && y_ < y + h;
    }
    template<typename V, typename W>
    bool is_on(const KRect<V, W>& rect) {
        return(
            (
                (rect.x >= x && rect.x < x + w) ||
                (rect.x + rect.w >= x && rect.x + rect.w < x + w)
            ) && (
                (rect.y >= y && rect.y < y + h) ||
                (rect.y + rect.h >= y && rect.y + rect.h < y + h)
            )
        );
    }
};

#endif
