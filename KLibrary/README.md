# KLibrary

KLibrary is a set of classes and functions providing basic resources to create a program and more specificaly for a game.
You can create a window by including "KWindow.hpp" and typing <code>KWindow my_window_object("window name", 1920, 1080);</code>.<br>
You can manage events by including "KEvent.hpp" and typing <code>KEvent my_event_object;</code> (this require a KWindow to work).<br>
Etc ...
<br>
<br>
The full documentation is here https://zero.freeboxos.fr/KLibrary .
<br>
<br>
A game example is available in "main.cpp.example", rename "main.cpp.example" to "main.cpp" and execute the make command.<br>

<br>
You can use it as you want, edit it as you want, redistribute it as you want and so on...<br>There is no obligation to cite the author or the source (but this is better if you do so).
