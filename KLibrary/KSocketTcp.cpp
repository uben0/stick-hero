#include <iostream>
#include <cstring>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <bitset>

#include "KSocketTcp.hpp"
#include "KTool.hpp"
#include "KTerm.hpp"

constexpr auto& socketTcp_close   = close;
constexpr auto& socketTcp_open    = socket;
constexpr auto& socketTcp_listen  = listen;
constexpr auto& socketTcp_accept  = accept;
constexpr auto& socketTcp_connect = connect;
constexpr auto& socketTcp_read    = read;
constexpr auto& socketTcp_write   = write;
constexpr auto& socketTcp_bind    = bind;

KSocketTcp::KSocketTcp(Propertie propertie) :
    KSocket(propertie),
    mListenning(false),
    mInQueue(0)
{}

KSocketTcp::KSocketTcp(const Addr& address, int queueLen, Propertie propertie) :
    KSocketTcp(propertie)
{
    open(address, queueLen);
}

KSocketTcp::~KSocketTcp() {
    close();
}

void KSocketTcp::reset(Propertie propertie) {
    if (mSocket != Closed) {
        if (shutdown(mSocket, SHUT_RDWR) != 0) {
            perror("unable to shutdown socket");
        }
        if (socketTcp_close(mSocket) != 0) {
            perror("unable to close socket");
        }
        verbose("closed");
    }
    mListenning = false;
    mInQueue    = 0;
    KSocket::reset(propertie);
}

void KSocketTcp::close() {
    if (mSocket != Closed) {
        if (shutdown(mSocket, SHUT_RDWR) != 0) {
            perror("unable to shutdown socket");
        }
        if (socketTcp_close(mSocket) != 0) {
            perror("unable to close socket");
        }
        verbose("closed");
    }
    mListenning = false;
    mInQueue    = 0;
    KSocket::reset(mPropertie);
}

bool KSocketTcp::open(IpVersion ipVersion) {
    /* if the socket is already opened it abort */
    if (mSocket != Closed) {
        verbose("open() exit : socket is already opened");
        return false;
    }
    if (ipVersion != Ipv4) {
        mSocket = socketTcp_open(Ipv6, SOCK_STREAM, 0);
    }
    if (mSocket == Closed) {
        mSocket = socketTcp_open(Ipv4, SOCK_STREAM, 0);
    }
    /* if socketTcp_open have succed */
    if (mSocket != Closed) {
        verbose("opened");
        /* apply socket propertie : broadcast right, non blocking, etc... */
        if (init_propertie() == false) {
            verbose("unable to init propertie");
            perror(nullptr);
        }
        mInQueue = 0;
        return true;
    }
    /* if socketTcp_open failed */
    verbose("unable to open");
    perror(nullptr);
    return false;
}

/* overload of open that automaticaly bind */
bool KSocketTcp::open(const Addr& address) {
    if (open(address.get_ip_version())) {
        return bind(address);
    }
    return false;
}

bool KSocketTcp::open(const Addr& address, int queueLen) {
    if (open(address)) {
        return listen(queueLen);
    }
    return false;
}

bool KSocketTcp::listen(int queueLen) {
    if (mSocket == Closed) {
        verbose("listen() exit : socket is not opened");
        return false;
    }
    if (mListenning) {
        verbose("listen() exit : socket is already listenning");
        return false;
    }
    if (socketTcp_listen(mSocket, queueLen) == -1) {
        verbose("listen() failed :");
        perror(nullptr);
        return false;
    }
    verbose("listenning");
    return (mListenning = true);
}

bool KSocketTcp::accept(KSocketTcp& client) const {
    if (mSocket == Closed) {
        verbose("accept() exit : socket is not opened");
        return false;
    }
    if (mListenning == false) {
        verbose("accept() exit : socket is not listenning");
        return false;
    }
    if (client.mSocket != Closed) {
        verbose("accept() exit : receiving socket is already opened");
        return false;
    }
    client.reset(client.mPropertie);
    socklen_t tmpAddrLen = sizeof client.mPeerAddr;
    client.mSocket = socketTcp_accept(mSocket, (sockaddr*)&client.mPeerAddr, &tmpAddrLen);
    if (client.mSocket == Closed) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
            verbose("no peer to accept");
        } else {
            perror(nullptr);
        }
        return false;
    }
    client.init_propertie();
    tmpAddrLen = sizeof client.mBindAddr;
    if (getsockname(client.mSocket, (sockaddr*)&client.mBindAddr, &tmpAddrLen) == -1) {
        verbose("unable to get bind address :");
        perror(nullptr);
    }
    verbose("peer " + client.mPeerAddr.to_string() + " have been accepted");
    return true;
}

bool KSocketTcp::connect(const Addr& address) {
    if (mSocket == Closed) {
        verbose("connect() exit : socket is not opened");
        return false;
    }
    if (address.is_set() == false) {
        verbose("connect() exit : address is empty");
        return false;
    }

    if (socketTcp_connect(mSocket, address.get_addr_ptr(), sizeof address) == Closed) {
        verbose("unbale to connect to " + address.to_string());
        perror(nullptr);
        return false;
    }
    mPeerAddr = address;
    socklen_t tmpAddrLen = sizeof mBindAddr;
    if (getsockname(mSocket, (sockaddr*)&mBindAddr, &tmpAddrLen) == -1) {
        verbose("unable to get bind address :");
        perror(nullptr);
    }
    verbose("connected to " + mPeerAddr.to_string());
    return true;
}

int KSocketTcp::in_queue() {
    if (mSocket != Closed && mInQueue == 0) {
        int state = socketTcp_read(mSocket, &mInQueue, sizeof mInQueue);
        if (state == 0) {
            verbose("lost connection");
            close();
        }
        if (state < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                verbose("no data to read");
            } else {
                perror(nullptr);
            }
        }
    }
    return mInQueue;
}

int KSocketTcp::read(void* buffer, int size) {
    if (mSocket != Closed && in_queue() > 0) {
        int state = 0;
        if (mInQueue > size) {
            verbose("read() warning : the packet is larger than the buffer, the exceeding part will remain in queue");
            state = socketTcp_read(mSocket, buffer, size);
            mInQueue -= size;
        } else {
            state = socketTcp_read(mSocket, buffer, mInQueue);
            mInQueue = 0;
        }
        verbose("received bytes : ", state);
        if (state == 0) {
            verbose("lost connection");
            close();
        } else
        if (state < 0) {
            perror(nullptr);
        }
        return state;
    }
    return 0;
}

int KSocketTcp::write(const char* message) {
    return write(message, strlen(message) + 1);
}

int KSocketTcp::write(const void* buffer, int size) {
    if (mSocket != Closed && buffer != nullptr && size > 0) {
        int state = socketTcp_write(mSocket, &size, sizeof size);
        if (state > 0) {
            state = socketTcp_write(mSocket, (void*)buffer, size);
            verbose("sent bytes : ", state);
        }
        if (state == Closed) {
            verbose("lost connection");
            perror(nullptr);
            close();
            state = 0;
        }
        return state;
    }
    return 0;
}

std::ostream& operator<<(std::ostream &ostream, KSocketTcp& socket) {
    char buffer[1025];
    buffer[                0] = '\0';
    buffer[sizeof buffer - 1] = '\0';
    socket.read(buffer, sizeof buffer - 1);
    return ostream << buffer;
}

std::istream& operator>>(std::istream &istream, KSocketTcp& socket) {
    char buffer[1024];
    istream.getline(buffer, sizeof buffer);
    socket.write(buffer);
    return istream;
}
