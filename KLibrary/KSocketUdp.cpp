#include <iostream>
#include <cstring>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <bitset>

#include "KSocketUdp.hpp"
#include "KTool.hpp"
#include "KTerm.hpp"

constexpr auto& socketUdp_close = close;
constexpr auto& socketUdp_open  = socket;
constexpr auto& socketUdp_read  = read;

KSocketUdp::KSocketUdp(Propertie propertie) : KSocket(propertie) {}

KSocketUdp::KSocketUdp(IpVersion ipVersion, Propertie propertie) :
KSocketUdp(propertie) {
    open(ipVersion);
}

KSocketUdp::KSocketUdp(const Addr& address, Propertie propertie) :
KSocketUdp(propertie) {
    open(address);
}

KSocketUdp::~KSocketUdp() {
    close();
}

void KSocketUdp::reset(Propertie propertie) {
    if (mSocket != Closed) {
        if (socketUdp_close(mSocket) != 0) {
            perror("unable to close socket");
        }
        else {
            verbose("closed");
        }
    }
    this->KSocket::reset(propertie);
}

void KSocketUdp::close() {
    if (mSocket != Closed) {
        if (socketUdp_close(mSocket) != 0) {
            perror("unable to close socket");
        }
        else {
            verbose("closed");
        }
    }
    this->KSocket::reset(mPropertie);
}

bool KSocketUdp::open(IpVersion ipVersion) {
    /* if the socket is already opened it abort */
    if (mSocket != Closed) {
        verbose("open() exit : socket is already opened");
        return false;
    }
    if (ipVersion != Ipv4) {
        mSocket = socketUdp_open(Ipv6, SOCK_DGRAM, 0);
    }
    if (mSocket == Closed) {
        mSocket = socketUdp_open(Ipv4, SOCK_DGRAM, 0);
    }
    /* if socketUdp_open have succed */
    if (mSocket != Closed) {
        verbose("opened");
        /* apply socket propertie : broadcast right, non blocking, etc... */
        if (init_propertie() == false) {
            verbose("unable to init propertie");
            perror(nullptr);
        }
        return true;
    }
    /* if socketUdp_open failed */
    verbose("unable to open");
    perror(nullptr);
    return false;
}

/* overload of open that automaticaly bind */
bool KSocketUdp::open(const Addr& address) {
    if (open(address.get_ip_version())) {
        return bind(address);
    }
    return false;
}

int KSocketUdp::read(void* buffer, int size) {
    if (mSocket != Closed && buffer != nullptr) {
        int state = socketUdp_read(mSocket, buffer, size);
        if (state == 0) {
            verbose("unable to read data");
        } else
        if (state < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                verbose("no data to read");
            } else {
                perror(nullptr);
            }
            state = 0;
        } else {
            verbose("received bytes : ", state);
        }
        return state;
    }
    return 0;
}

int KSocketUdp::read(Addr& address, void* buffer, int size) {
    if (mSocket != Closed) {
        socklen_t socklen = sizeof address;
        int state = recvfrom(mSocket, buffer, size, 0, (sockaddr*)&address, &socklen);
        if (state < 0) {
            state = 0;
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                verbose("no data to read");
            } else {
                perror(nullptr);
            }
        } else
        if (state > 0) {
            verbose("received bytes : ", state);
        } else {
            verbose("unable to read data");
        }
        return state;
    }
    return 0;
}

int KSocketUdp::read_and_catch(void* buffer, int size) {
    return read(mPeerAddr, buffer, size);
}

int KSocketUdp::read_and_catch(Addr& address, void* buffer, int size) {
    int state = read(mPeerAddr, buffer, size);
    address = mPeerAddr;
    return state;
}

int KSocketUdp::write(const Addr& address, const void* buffer, int size) {
    if (mSocket != Closed && buffer != nullptr && address.is_set()) {
        int state = sendto(
            mSocket, buffer, size, 0,
            address.get_addr_ptr(),
            sizeof address
        );
        if (state == Closed) {
            verbose("unable to send data");
            state = 0;
        }
        else {
            verbose("sent bytes : ", state);
        }
        return state;
    } else {
        verbose("unable to send data");
    }
    return 0;
}

int KSocketUdp::write(Addr const& address, const char* message) {
    return write(address, message, strlen(message) + 1);
}

int KSocketUdp::write(const char* message) {
    if (mPeerAddr.is_set()) {
        return write(mPeerAddr, message);
    }
    return 0;
}

int KSocketUdp::write(const void* buffer, int size) {
    if (mPeerAddr.is_set()) {
        return write(mPeerAddr, buffer, size);
    }
    return 0;
}

void KSocketUdp::set_peer_addr(const Addr& address) {
    mPeerAddr = address;
}

std::ostream& operator<<(std::ostream &ostream, KSocketUdp& socket) {
    char buffer[1025];
    buffer[                0] = '\0';
    buffer[sizeof buffer - 1] = '\0';
    socket.read_and_catch(buffer, sizeof buffer - 1);
    return ostream << buffer;
}

std::istream& operator>>(std::istream &istream, KSocketUdp& socket) {
    char buffer[1024];
    istream.getline(buffer, sizeof buffer);
    socket.write(buffer);
    return istream;
}
