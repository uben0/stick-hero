#include "KBoard.hpp"
#include <iostream>

KBoard::KBoard() :
    mWindow(nullptr),
    mSurface(nullptr),
    w(0),
    h(0),
    id(0)
{}

KBoard::KBoard(int width, int height) :
    KBoard()
{
    open(width, height);
}

KBoard::~KBoard() {
    close();
}

bool KBoard::is_opened() {
    return mWindow != nullptr;
}

bool KBoard::is_closed() {
    return mWindow == nullptr;
}

bool KBoard::open(int width, int height) {
    assert(is_closed());
    mWindow = SDL_CreateWindow("board", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, 0);
    if (mWindow == nullptr) {
        return false;
    }
    SDL_SetWindowData(mWindow, "this", this);
    mSurface = SDL_GetWindowSurface(mWindow);
    if (mSurface == nullptr) {
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;
        return false;
    }
    *((int*)&w ) = width;
    *((int*)&h ) = height;
    *((int*)&id) = SDL_GetWindowID(mWindow);
    return true;
}

void KBoard::display() {
    SDL_UpdateWindowSurface(mWindow);
}

void KBoard::close() {
    if (mWindow != nullptr) {
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;
        *((int*)&w ) = 0;
        *((int*)&h ) = 0;
        *((int*)&id) = 0;
    }
}

void KBoard::print(const KSurface& src, int x, int y) {
    SDL_Rect rect = {
        x,
        h - y - src.h,
        src.w,
        src.h
    };
    assert(src.get_ptr() != nullptr);
    SDL_BlitSurface(src.get_ptr(), nullptr, mSurface, &rect);
    display();
}

void KBoard::fill(const KRect<>& rect, KColor color) {
    assert(mSurface != nullptr);
    SDL_Rect sdlRect = {
        rect.x,
        h - rect.y - rect.h,
        rect.w,
        rect.h
    };
    SDL_FillRect(mSurface, &sdlRect, SDL_MapRGB(mSurface->format, color.r, color.g, color.b));
    display();
}

void KBoard::fill(KColor color) {
    fill({0, 0, w, h}, color);
}

void KBoard::wait_closure() {
    SDL_Event e;
    bool loop = true;
    while (loop) {
        if (SDL_PollEvent(&e)) {
            if (e.type == SDL_WINDOWEVENT && e.window.windowID == id && e.window.event == SDL_WINDOWEVENT_CLOSE) {
                loop = false;
            }
        }
        else {
            SDL_Delay(100);
        }
    }
}
