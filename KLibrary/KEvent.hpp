#ifndef KEvent_HEADER_DEFINED
#define KEvent_HEADER_DEFINED

#include "KKey.hpp"

class KEvent {
    bool key[KKey::Num];
public:
    static constexpr bool DetectRepeat = true;
    struct {
        bool clicked;
        int x, y;
    } mouseLeft, mouseMiddle, mouseRight;
    int close;
    bool quit;

    KEvent();
    void update(bool repeat = false);
    bool operator[](KKey::Id keyId) const;
};

#endif
