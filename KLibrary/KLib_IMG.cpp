#include <iostream>
#include <cassert>
#include "KTerm.hpp"
#include "KLib_IMG.hpp"

int KLib_IMG::instances = 0;

void KLib_IMG::verbose(const std::string& message) {
    std::cout << KTerm::Bold << "[" << KTerm::Color<117> << "KLib_IMG"
    << KTerm::UColor << "]" << KTerm::UBold << " " << KTerm::Color<117> << message
    << KTerm::Def << std::endl;
}

KLib_IMG::KLib_IMG() {
    if (instances == 0) {
        if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF) == 0) {
            verbose("enable to initialize");
            exit(-1);
        }
        verbose("initialized");
    }
    instances++;
}

KLib_IMG::~KLib_IMG() {
    instances--;
    if (instances == 0) {
        IMG_Quit();
        verbose("quit");
    }
}
