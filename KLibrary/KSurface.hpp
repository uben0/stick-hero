#ifndef KSurface_HEADER_DEFINED
#define KSurface_HEADER_DEFINED

#include "KLib_SDL.hpp"
#include "KLib_IMG.hpp"
#include "KColor.hpp"
#include "KRect.hpp"

class KSurface {
    KLib_IMG img_init;
    SDL_Surface* mSurface;
public:
    void unlock_write();
    const int w, h;
    KSurface();
    KSurface(const KSurface& src);
    KSurface(SDL_Surface* src);
    KSurface(const std::string& fileName);
    KSurface(int width, int height);
    KSurface(int width, int height, KColor color);
    ~KSurface();
    bool is_empty() const;
    SDL_Surface* get_ptr() const;
    KSurface& operator=(const KSurface& src);
    void fill(const KRect<>& rect, KColor color);
    void fill(                     KColor color);
    void print(const KSurface& src, int x, int y);
    KColor get_pixel(int x, int y) const;
    void   set_pixel(int x, int y, KColor color);
    void show() const;
    void save(const std::string& fileName) const;
    KSurface smaller_copy(int factor) const;
    KSurface bigger_copy(int factor) const;
    void blur(int radius);
    void blur_strong(int factor);
    void darker(unsigned char factor);
    inline const KColor* operator[](int y) const {
        return ((KColor*)mSurface->pixels) + y * w;
    }
    inline KColor* get_map() {
        unlock_write();
        return (KColor*)mSurface->pixels;
    }
    inline const KColor* get_const_map() const {
        return (const KColor*)mSurface->pixels;
    }
};

#endif
