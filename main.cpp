#include "KLibrary/KLibrary.hpp"
#include <vector>

int main(int argc, char const *argv[]) {
    KColor colorBack(210, 210, 210), colorFront(40, 40, 40);

    KWindow window("stick hero", 420, 520);
    window.fill(colorFront);
    window.display();

    constexpr int tileSize = 10;
    constexpr int plotHeight = 4 * tileSize;
    KFont font("pixeled.ttf", 2 * tileSize);
    auto labelGameOver = window.texture(font("GAME OVER", colorFront));

    float animationRatio = 0;
    int score = 0;

    auto labelScore = window.texture(font(std::to_string(score), colorFront));

    enum State {
        STATE_INIT,
        STATE_WAIT,
        STATE_GROW,
        STATE_FALL,
        STATE_FORWARD,
        STATE_GAME_OVER
    } gameState = STATE_INIT;

    std::vector<KRect<float, int>> block;
    std::vector<KRect<float>> stick;

    KEvent event;
    KControl control;
    KTimer timer(KTimer::Fps60);
    while (!event.close && !event.quit && !event[KKey::Escape]) {
        event.update();
        control.update();

        if (gameState == STATE_INIT) {
            score = 0;
            labelScore = window.texture(font(std::to_string(score), colorFront));

            block.clear();
            stick.clear();

            block.insert(block.end(), { 0, 0, 4 * tileSize, plotHeight});
            while (block[block.size() - 1].x < window.w) {
                auto& rect = block[block.size() - 1];
                block.insert(block.end(), {rect.x + rect.w + rand() % (4 * tileSize) + 2 * tileSize, 0, rand() % (4 * tileSize) + 2 * tileSize, plotHeight});
            }

            gameState = STATE_WAIT;
        }
        else if (gameState == STATE_WAIT) {
            if (event[KKey::Space]) {
                stick.insert(stick.end(), {2 * tileSize, plotHeight, tileSize * 2 / 5, 0});
                auto& rect = block[block.size() - 1];
                block.insert(block.end(), {rect.x + rect.w + rand() % (4 * tileSize) + 2 * tileSize, 0, rand() % (4 * tileSize) + 2 * tileSize, plotHeight});
                gameState = STATE_GROW;
                stick[0].h = 0;
            }
        }
        else if (gameState == STATE_GROW) {
            if (control[KKey::Space]) {
                stick[0].h += timer.ms * 0.02 * tileSize;
            } else {
                gameState = STATE_FALL;
            }
        }
        else if (gameState == STATE_FALL) {
            stick[0] = KRect<float, int>(stick[0].x + stick[0].w, stick[0].y, stick[0].h, stick[0].w);
            if (stick[0].w + stick[0].x > block[1].x && stick[0].w + stick[0].x < block[1].x + block[1].w) {
                animationRatio = 0;
                score++;
                labelScore = window.texture(font(std::to_string(score), colorFront));
                gameState = STATE_FORWARD;
            }
            else {
                gameState = STATE_GAME_OVER;
            }
        }
        else if (gameState == STATE_FORWARD) {
            static int shift = 0;
            if (animationRatio == 0) {
                shift = (block[1].x + block[1].w - 2 * tileSize);
            }
            else if (animationRatio < 1) {
                for (unsigned i = 0; i < block.size(); i++) {
                    block[i].x -= shift * 0.005 * timer.ms;
                }
                for (unsigned i = 0; i < stick.size(); i++) {
                    stick[i].x -= shift * 0.005 * timer.ms;
                }
            }
            animationRatio += 0.005 * timer.ms;
            if (animationRatio >= 1) {
                block.erase(block.begin());
                stick.erase(stick.begin());
                gameState = STATE_WAIT;
            }
        }
        else if (gameState == STATE_GAME_OVER) {
            if (event[KKey::Return]) {
                gameState = STATE_INIT;
            }
        }

        window.fill(colorBack);
        for (unsigned i = 0; i < block.size(); i++) {
            window.fill(block[i], colorFront);
        }
        for (unsigned i = 0; i < stick.size(); i++) {
            window.fill(stick[i], colorFront);
        }
        window.fill({tileSize / 2, plotHeight + 2 * tileSize / 10, tileSize, 2 * tileSize}, colorFront);
        window.fill({tileSize / 2, plotHeight + 15 * tileSize / 10, tileSize, tileSize / 4}, KColor::Red);
        if (gameState == STATE_GAME_OVER) {
            window.print(labelGameOver, window.w / 2 - labelGameOver.w / 2, window.h * 2 / 3 - labelGameOver.h / 2);
        }
        window.print(labelScore,  window.w / 2 - labelScore.w / 2, window.h / 3 - labelScore.h / 2);
        window.display();
        timer.wait();
    }
    return 0;
}
