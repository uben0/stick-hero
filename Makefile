# C++ recursive makefile

CXX = g++
BIN = built

SRC  = $(shell find * -name '*.cpp')
HEAD = $(shell find * -name '*.hpp')
OBJ  = $(patsubst %.cpp,%.o, $(SRC))

CXXFLAGS = -Wall -std=c++17
LIBS     = $(shell pkg-config --libs sdl2 SDL2_ttf SDL2_image)

debug: CXXFLAGS += -g
debug: $(BIN)
	./$(BIN)

release: $(BIN)

clean:
	rm -rf $(OBJ) $(BIN) $(SAVE)

$(BIN): $(OBJ)
	$(CXX) $(OBJ) $(LIBS) -o $(BIN)

%.o: %.cpp $(HEAD)
	$(CC) -c -o $@ $< $(CXXFLAGS)
